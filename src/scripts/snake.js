document.addEventListener("DOMContentLoaded", function (e) {

    let canvas = document.getElementById('gameboard');
    let scoreBoard = document.querySelector('.scoreboard');
    let highscoreLabel = document.querySelector('.highscore');
    let restartBtn = document.getElementById('restart');
    let gameOverScreen = document.querySelector('.gameover-screen');
    let finalScoreLabel = document.querySelector('.final-score');
    let playerNameInput = document.querySelector('.player-name');
    let highscoreBoardBtn = document.getElementById('highscores-btn');
    let highscoresBoard = document.querySelector('.highscores-board');
    let highscoresList = document.querySelector('.highscores-list');
    let highscoresReturnBtn = document.getElementById('return-from-highscores');

    canvas.width = 400;
    canvas.height = 400;
    let ctx = canvas.getContext('2d');

    let isPlayerSaved,
        state,
        points,
        playerName,
        snakeBody,
        direction,
        blockSize,
        snakeHead,
        apple;

    let init = function () {
        canvas.style.display = "block";
        gameOverScreen.style.display = "none";
        //eliminate duplicates from highscore board;
        //if it's value's true,same player can't be saved again
        isPlayerSaved = false;

        state = 'game';
        points = 0;

        playerName = '';

        snakeBody = [{
            x: 60,
            y: 60,
            color: 'yellow',
            lastPosX: null,
            lastPosY: null
        }];

        for (let i = 0; i < 5; i++) {
            snakeBody.push({
                color: 'yellow'
            });
        }

        direction = 'right';
        blockSize = 20;
        snakeHead = snakeBody[0];

        apple = {
            x: Math.floor(Math.random() * canvas.width),
            y: Math.floor(Math.random() * canvas.height),
            color: '',
        };
    }
    init();

    let draw = function (o) {
        if (o.x != undefined || o.x != null) {
            ctx.beginPath();
            ctx.fillStyle = o.color;
            ctx.fillRect(o.x, o.y, blockSize, blockSize);
        }
    }

    let inputHandler = function (e) {
        if (direction === 'right' && e.keyCode === 37) {
            return;
        } else if (direction === 'left' && e.keyCode === 39) {
            return;
        } else if (direction === 'up' && e.keyCode === 40) {
            return;
        } else if (direction === 'down' && e.keyCode === 38) {
            return;
        }

        switch (e.keyCode) {
            case 38:
                /* Up arrow was pressed */
                direction = 'up';
                break;
            case 40:
                /* Down arrow was pressed */
                direction = 'down';
                break;
            case 37:
                /* Left arrow was pressed */
                direction = 'left';
                break;
            case 39:
                /* Right arrow was pressed */
                direction = 'right';
                break;
        }
    }

    let move = function () {
        switch (direction) {
            case 'left':
                snakeHead.x -= blockSize;
                break;
            case 'right':
                snakeHead.x += blockSize;
                break;
            case 'down':
                snakeHead.y += blockSize;
                break;
            case 'up':
                snakeHead.y -= blockSize;
                break;
        }
    }

    let update = function () {
        scoreBoard.innerHTML = "Points: " + points;

        //Calculate the highscore and display it
        if (localStorage.getItem('highscores')) {
            let storageHighscoresItemsArr = [];
            let storageHighscoresItems = JSON.parse(localStorage.getItem('highscores'));
            for (let i = 0; i < storageHighscoresItems.length; i++) {
                storageHighscoresItemsArr.push(storageHighscoresItems[i].score);
            }

            let highscore = Math.max(...storageHighscoresItemsArr);
            highscoreLabel.innerHTML = "Wynik: " + highscore;
        }

        //Snake's body movement
        for (let i = 1; i < snakeBody.length; i++) {
            snakeBody[i].x = snakeBody[i - 1].lastPosX;
            snakeBody[i].y = snakeBody[i - 1].lastPosY;

            snakeBody[i - 1].lastPosX = snakeBody[i - 1].x;
            snakeBody[i - 1].lastPosY = snakeBody[i - 1].y;
        }
    }

    let generateApple = function () {
        apple = {
            x: Math.floor(Math.random() * canvas.width),
            y: Math.floor(Math.random() * canvas.height),
            color: 'red',
        };

        let isAppleReachable = false;

        let isAppleOutsideBody = function () {
            for (let i = 0; i < snakeBody.length; i++) {
                if ((snakeBody[i].x === apple.x) && (snakeBody[i].y === apple.y)) {
                    return false;
                }
            }
            return true;
        }

        while (isAppleReachable === false) {
            if (((apple.x % blockSize === 0) && (apple.y % blockSize === 0)) && isAppleOutsideBody()) {
                isAppleReachable = true;
            } else {
                apple.x = Math.floor(Math.random() * canvas.width);
                apple.y = Math.floor(Math.random() * canvas.height);
            }
        }
    }

    let checkForColliions = function () {
        if (snakeHead.x >= canvas.width) {
            snakeHead.x = 0;
        } else if (snakeHead.x <= -10) {
            snakeHead.x = canvas.width;
        } else if (snakeHead.y <= -10) {
            snakeHead.y = canvas.height;
        } else if (snakeHead.y >= canvas.height) {
            snakeHead.y = 0;
        }

        if ((snakeHead.x === apple.x) && (snakeHead.y === apple.y)) {
            points += 10;
            generateApple();
            snakeBody.push({
                color: 'yellow'
            });
        }

        for (let i = 1; i < snakeBody.length; i++) {
            if ((snakeHead.x === snakeBody[i].x) && (snakeHead.y === snakeBody[i].y)) {
                state = 'gameOver';
            }
        }
    }

    let savePlayerToLocalStorage = function () {
        if (isPlayerSaved === false) {
            isPlayerSaved = true;
            playerName = playerNameInput.value;
            let highscores = [];
            if (localStorage.getItem('highscores')) {
                highscores = highscores.concat(JSON.parse(localStorage.getItem('highscores')));
            }
            highscores.push({
                name: playerName,
                score: points
            });
            localStorage.setItem('highscores', JSON.stringify(highscores));
        }
    }

    let loop = function () {
        if (state === 'game') {
            update();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            snakeBody.forEach((s) => {
                draw(s);
            })
            draw(apple);
            for (let y = blockSize; y < 400; y += blockSize) {
                ctx.beginPath();
                ctx.moveTo(0, y);
                ctx.lineTo(400, y);
                ctx.stroke();
            }
            for (let x = blockSize; x < 400; x += blockSize) {
                ctx.beginPath();
                ctx.moveTo(x, 0);
                ctx.lineTo(x, 400);
                ctx.stroke();
            }
            move();
            checkForColliions();
            setTimeout(loop, 100);
        } else if (state === 'gameOver') {
            canvas.style.display = "none";
            gameOverScreen.style.display = "block";
            highscoresBoard.style.display = "none";

            finalScoreLabel.innerHTML = "Twój wynik: " + points;
        } else if (state === 'highscores') {
            gameOverScreen.style.display = 'none';
            highscoresBoard.style.display = 'flex';
            highscoresList.innerHTML = '';

            let storageHighscoresItems = JSON.parse(localStorage.getItem('highscores'));
            let storageHighscoresItemsArr = [];
            for (let i = 0; i < storageHighscoresItems.length; i++) {
                storageHighscoresItemsArr.push(storageHighscoresItems[i]);
            }

            storageHighscoresItemsArr.sort(function (a, b) {
                if (a.score > b.score) {
                    return 1;
                }
                if (a.score < b.score) {
                    return -1;
                }

                return 0;
            });
            storageHighscoresItemsArr.reverse();

            for (let i = 0; i < 10; i++) {
                if (storageHighscoresItemsArr[i]) {
                    let li = document.createElement('li');
                    highscoresList.appendChild(li);
                    li.innerHTML = (storageHighscoresItemsArr[i].name || 'Noname') + " - " + storageHighscoresItemsArr[i].score;
                }
            }
        }
    }
    loop();
    generateApple()
    addEventListener('keydown', e => inputHandler(e), true);

    restartBtn.addEventListener('click', () => {
        savePlayerToLocalStorage();
        init();
        generateApple();
        loop();
    })

    highscoreBoardBtn.addEventListener('click', () => {
        savePlayerToLocalStorage();
        state = 'highscores';
        loop();
    })

    highscoresReturnBtn.addEventListener('click', () => {
        state = 'gameOver';
        loop();
    })
});