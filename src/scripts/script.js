document.addEventListener("DOMContentLoaded", function (e) {
    //menu mobile
    function hideMenu() {
        let menuIcon = document.querySelector(".left__elem--hamburger");
        let closeIcon = document.querySelector(".left__elem--close");
        let menuMobile = document.querySelector(".menu__mobile");
        let overlay = document.querySelectorAll(".mask");
        // let menuWidth = menuMobile.clientWidth;

        menuIcon.addEventListener("click", function () {
            if (menuIcon.className !== "unactive") {
                menuMobile.classList.add("open");
                closeIcon.classList.toggle("unactive");
                menuIcon.classList.toggle("unactive");
                overlay[0].classList.toggle("active");
            }
        });

        closeIcon.addEventListener("click", function () {
            if (closeIcon.className !== "active") {
                menuMobile.classList.remove("open");
                menuIcon.classList.toggle("unactive");
                closeIcon.classList.toggle("unactive");
                overlay[0].classList.toggle("active");
            }
        });
    }
    hideMenu();

    // drugi poziom menu w menu mobile
    function menuExpand() {
        let show = document.querySelector(".elem--show");
        let expandEl = document.querySelector(".expand--height0");
        // let h = expandEl.scrollHeight;
        show.addEventListener("click", function (e) {
            expandEl.classList.toggle("open");

        });
    }
    menuExpand();



    // //zamykanie menu po kliknięciu poza nim
    // function clickOutsideToCloseMenu() {
    //     let closeIcon = document.querySelector(".left__elem--close");
    //     let menuMobile = document.querySelector(".menu__mobile");


    //     document.addEventListener("click", function (event) {
    //         // If user clicks inside the element, do nothing
    //         if (event.target.closest(".left__elem--close")) return;

    //         // If user clicks outside the element, hide it!
    //         menuMobile.classList.remove("open");
    //     });
    // }
    // clickOutsideToCloseMenu();


});